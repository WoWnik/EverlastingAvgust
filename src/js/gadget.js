var dagDisplay, datumDisplay, maandjaarDisplay;

function debug(str) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:3000/test?' + str, false);
    xhr.send();
}

var timesOfDay = {
    sunrise: {
        backgroundImage: "images/sunset.png", // Yea, one pic for 2 diff times
        textColor: "white"
    },
    day: {
        backgroundImage: "images/day.png",
        textColor: "white"
    },
    sunset: {
        backgroundImage: "images/sunset.png",
        textColor: "white"
    },
    night: {
        backgroundImage: "images/night.png",
        textColor: "white"
    }
}

var dayNames = ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"];
var monthNames = ["Июнь", "Июль", "Август"];

function getCurrentTimeOfDay() {
    var hour = (new Date).getHours();

    if ((hour > 5) && (hour <= 9)) {
        return timesOfDay.sunrise;
    } else if ((hour > 9) && (hour <= 17)) {
        return timesOfDay.day;
    } else if ((hour > 17) && (hour <= 20)) {
        return timesOfDay.sunset;
    }
    return timesOfDay.night;
}

function getDateText() {
    var currentDate = new Date();
    var avgustYear = currentDate.getMonth() <= 4 ? currentDate.getYear() - 1 : currentDate.getYear();
    var avgustDate = new Date(avgustYear, 6, 31, currentDate.getHours(), currentDate.getMinutes(), currentDate.getSeconds(), currentDate.getMilliseconds());

    function getMonthName() {
        if (currentDate.getMonth() === 5) {
            return monthNames[0];
        } else if (currentDate.getMonth() === 6) {
            return monthNames[1];
        }
        return monthNames[2];
    }

    function getDayCount() {
        if (currentDate.getMonth() === 5 || currentDate.getMonth() === 6) {
            return currentDate.getDate();
        }
        return (currentDate - avgustDate) / (1000 * 60 * 60 * 24);
    }

    return {
        dayName: dayNames[currentDate.getDay()],
        dayCount: getDayCount(),
        monthName: getMonthName(),
        yearCount: avgustDate.getYear()
    }
}

function getSize() {
    if (System.Gadget.docked) {
        return {
            width: "130px",
            height: "141px"
        }
    }
    return {
        width: "210px",
        height: "228px"
    }
}

function render() {
    try {
        System.Gadget.beginTransition();
        background.removeObjects();

        var size = getSize();
        document.body.style.width = size.width;
        document.body.style.height = size.height;
        background.style.width = size.width;
        background.style.height = size.height;

        var currentTimeOfDay = getCurrentTimeOfDay();
        var dateText = getDateText();
        background.src = currentTimeOfDay.backgroundImage;
        var color = currentTimeOfDay.textColor;

        if (System.Gadget.docked) {
            dagDisplay = background.addTextObject(dateText.dayName, "Segoe UI", 18, color, 65, 17);
            datumDisplay = background.addTextObject(dateText.dayCount, "Segoe UI", 70, color, 65, 17);
            maandjaarDisplay = background.addTextObject(dateText.monthName + " " + dateText.yearCount, "Segoe UI", 18, color, 65, 110);
        } else {
            dagDisplay = background.addTextObject(dateText.dayName, "Segoe UI", 23, color, 105, 20);
            datumDisplay = background.addTextObject(dateText.dayCount, "Segoe UI", 120, color, 105, 15);
            maandjaarDisplay = background.addTextObject(dateText.monthName + " " + dateText.yearCount, "Segoe UI", 23, color, 105, 178);
        }
        dagDisplay.align = 1;
        datumDisplay.align = 1;
        maandjaarDisplay.align = 1;
        maandjaarDisplay.addGlow("gray", 2, 80);
    } catch (e) {
        debug(e);
        debug(e.message);
    }

    setTimeout(render, 30000);
}

function init() {
    System.Gadget.onUndock = render;
    System.Gadget.onDock = render;
    render();
}

init();
