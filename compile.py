import os
import zipfile


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), os.path.join(root[4:], file))


if __name__ == '__main__':
    zipf = zipfile.ZipFile('dist/Everlasting Avgust.gadget', 'w', zipfile.ZIP_DEFLATED)
    zipdir('src', zipf)
    zipf.close()
