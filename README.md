# Everlasting Avgust

Гаджет рабочего стола, который показывает текущий день августа.
Для установки необходимо скачать и установить DesktopGadgetsRevived, затем установить dist/Everlasting Avgust.gadget

<img src="images/demo.png"  alt="Everlasting Avgust"/>

Старые версии <a href="https://drive.google.com/drive/folders/0B1XzA8mGhC0HYkRudzNfOWx6VWc">здесь</a>
